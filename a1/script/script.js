/*Activity:
1. Update and Debug the following codes to ES6
		Use template literals
		Use array/object destructuring
		Use arrow function*/

let student1 = {
	name    : "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101 ", "Social Sciences 201"]
}

let student2 = {
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401 ", "Natural Sciences 402"]
}

// function introduce(student){

// 	//Note: You can destructure objects inside functions.

// 	console.log("Hi! " + "I'm " + student.name + " ." + " I am " + student.ages + " years old.");
// 	console.log("I study the following courses + " classes);
// }

const introduce = (student) => {
	console.log(`Hi! I'm ${student.name}. I am ${student.age} years old. I study the following courses ${student.classes}`);
}

introduce(student1);
introduce(student2);



// function getCube(num){
// 	console.log(Math.pow(num,3));
// }


const getCube = (num) => Math.pow(num,3);
console.log(cube = getCube(3));


let numArr = [15,16,32,21,21,2];

// numArr.forEach(function(num){
// 	console.log(num);
// })

numArr.forEach(num => console.log(num));

let numSquared = numArr.map(num => console.log(Math.pow(num,2)));

/*2. Create a class constructor able to receive 3 arguments
		- it should be able to receive two strings and a number
		- Using the this keyword assign properties:
			name,
			breed,
			dogAge = <7 * human years>
				- assign the parameters as values to each property.*/

class Dog {
	constructor(name, breed,dogAge){
		this.name = name;
		this.breed = breed;
		this.dogAge = dogAge * 7;
	}
}

let dog1 = new Dog ('Sophie', 'Shitzu', 9);
console.log(dog1);


/*3. Create 2 new objects using our class constructor
	This constructor should be able to create Dog objects.
	Log the 2 new Dog objects in the console or alert.*/

let dog2 = new Dog ('Brownie', 'Chihuahua', 6);
let dog3 = new Dog ('Paningning', 'Shitzu', 3);

console.log(dog2);
console.log(dog3)	;